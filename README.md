# sql4csv

This application allows you to query a group of related CSV files using SQL.  Queries can be performed either from a command-line interface (using the H2 shell), or from a GUI interface using a browser to access the H2 web server.

This application is based on the [Scala CLI starter application](https://gitlab.com/lfrost/scala-cli-starter).  See that README for instructions on building and running (replacing scala-cli-starter with sql4csv in those instructions).


## Data

Multiple CSV files can be loaded in the same session, each mapping to a single table.  The filename with the extension discarded will be used as the table name.  Anything after a dash will also be discarded (to allow for filenames of the format table-timestamp.csv or table-version.csv).

Each CSV file must have a header, and the header value for each column will be used for the column name in the table.  Optionally, SQL data types may be embedded in the header by specifying headers of the format name::dataType.  The default data type is `varchar_ignorecase`.  See [H2 SQL data types](http://www.h2database.com/html/datatypes.html) for a list of data types.


## Running

sql4csv has command line options for selecting different CSV formats, end-of-line styles, and character sets.

```bash
$ target/scala-2.13/sql4csv-1.1.0 --help
sql4csv 1.1.0
Usage: sql4csv [options] <csvFile> ...

  --help              prints this usage text
  --version           display version then exit
  --verbose           verbose output
  --charset <value>   character set, with utf-8 as the default
  --eolstyle <value>  EOL style (unix|mac|dos), with unix as the default
  --format <value>    input file format (csv|tsv), with csv as the default
  <csvFile> ...       CSV input files
```

Use the following command to run sql4csv with the included sample data.

```bash
target/scala-2.13/sql4csv-1.1.0 --verbose data/*.csv
```

Once the CSV data has been loaded, the following commands are available.

```
sql4csv> help
help   Display this help information
shell  Launch an SQL shell
web    Launch a SQL GUI client in a browser.
exit   Exit the application
sql4csv>
```

The `shell` command will launch an H2 shell connected to the loaded data.  The `web` command will launch an H2 web client connected to the data.

See `doc/sample_session.txt` for a sample session using the H2 shell.


## Relevant Documentation

* [H2 SQL data types](http://www.h2database.com/html/datatypes.html)
* [H2 SQL grammar](http://www.h2database.com/html/grammar.html)
