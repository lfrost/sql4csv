/*
 * Copyright (c) 2017-2019 Lyle Frost <lfrost@cnz.com>.
 */

import java.sql.{Connection => DbConnection}

import main.{ProcessLine, Sql}
import org.scalatest.TryValues
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

/** ProcessLine tests
 */
class ProcessLineSpec extends AnyFlatSpec with TryValues with Matchers {
    // Test fixture
    private def withDatabase(testCode:DbConnection => Any) = {
        val dbConnection = Sql.connect().get
        testCode(dbConnection)
    }

    "The sentence 'done'" should "be true" in withDatabase { implicit dbConnection =>
        val tryResult = ProcessLine("done")
        tryResult.isSuccess     should be (true)
        tryResult.success.value should be (true)
    }

    "The sentence 'Done.'" should "be true" in withDatabase { implicit dbConnection =>
        val tryResult = ProcessLine("Done.")
        tryResult.isSuccess     should be (true)
        tryResult.success.value should be (true)
    }

    "The sentence 'Invalid command.'" should "be false" in withDatabase { implicit dbConnection =>
        val tryResult = ProcessLine("Invalid command.")
        tryResult.isSuccess should be (false)
    }
}
