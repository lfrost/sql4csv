/*
 * Copyright (c) 2017-2019 Lyle Frost <lfrost@cnz.com>.
 */

package main

import java.sql.{Connection => DbConnection, DriverManager}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.Try

import anorm.{NamedParameter, SQL}

/** Execute SQL statements
 *
 *  @author Lyle Frost <lfrost@cnz.com>
 */
object Sql {
    // See http://www.h2database.com/html/features.html#database_url
    private val JdbcUrl    = "jdbc:h2:mem:tmp"
    private val JdbcUser   = "sa"
    private val JdbcPass   = "sapass"
    private val JdbcDriver = "org.h2.Driver"

    /** List separator string for lists of column names, values, etc.
     */
    protected val ListSep = ", "

    /** Characters to strip from identifiers (regex)
     */
    final protected val CharsToStripFromIdentifier = "[{}\"'.\\\\ ]"

    /** Characters to strip from placeholder names (regex)
     */
    final protected val CharsToStripFromPlaceholder = "[{}\"'\\\\ ]"

    /** Quote character for identifiers
     */
    val QuoteId = '"'

    /** Quote character for string literal values
     */
    val QuoteVal = '\''

    /** Default data type
     */
    val DefaultType = "varchar_ignorecase"

    /** Connect to a temporary database
     *
     *  @return Database connection or exception
     */
    def connect() : Try[DbConnection] = Try {
        Class.forName(JdbcDriver)
        DriverManager.getConnection(JdbcUrl, JdbcUser, JdbcPass)
    }

    /** Launch database shell
     *
     *  @return Unit or exception
     */
    def shell() : Try[Unit] = Try {
        org.h2.tools.Shell.main(
            "-url",      JdbcUrl,
            "-user",     JdbcUser,
            "-password", JdbcPass,
            "-driver",   JdbcDriver
        )
    }

    /** Start a web server and launch a browser client.
     *
     *  The server will automatically stop when the client disconnects,
     *  but the disconnect button on the web page must be pressed.
     *
     *  @param dbConnection Database connection
     *  @return             Unit or exception
     */
    def web()(implicit dbConnection:DbConnection) : Try[Unit] = Try {
        Future {
            org.h2.tools.Server.startWebServer(dbConnection)
        }
        ()
    }

    /* ============================================================== */
    /* Lists and individual identifiers and placeholders              */
    /* ============================================================== */

    /** Generate an SQL string of identifiers for an INSERT (without parentheses)
     *
     *  @param columns Columns
     *  @return        SQL INSERT string
     */
    def sqlInsertColumns(columns:Seq[String]) : String = {
        columns match {
            case Nil              => ""
            case cols:Seq[String] => cols.map(sqlQuoteIdentifier(_)).reduceLeft(_ + ListSep + _)
        }
    }

    /** Generate an SQL string for a single parameter placeholder
     *
     *  @param s Parameter
     *  @return  SQL parameter placeholder as a string
     */
    final def sqlParameterPlaceholder(s:String) : String = {
        "{" + sqlParameterName(s) + "}"
    }

    /** Generate an SQL string of value placeholders for an INSERT (without parentheses)
     *
     *  @param columns List of columns
     *  @return        SQL INSERT value string
     */
    def sqlInsertValueParameters(columns:Seq[String]) : String = {
        columns match {
            case Nil              => ""
            case cols:Seq[String] => cols.map(sqlParameterPlaceholder(_)).reduceLeft(_ + ListSep + _)
        }
    }

    /** Quote an SQL identifier
     *
     *  The identifier may be either a column name or table-name.column-name.
     *
     *  @param s Identifier to quote
     *  @return  Quoted identifier
     */
    def sqlQuoteIdentifier(s:String) : String = {
        s.split('.') match {
            case Array(table, column) => {
                s"$QuoteId${table.replaceAll(CharsToStripFromIdentifier, "")}$QuoteId.$QuoteId${column.replaceAll(CharsToStripFromIdentifier, "")}$QuoteId"
            }
            case Array(column)        => {
                s"$QuoteId${column.replaceAll(CharsToStripFromIdentifier, "")}$QuoteId"
            }
            case _                    => {
                // Invalid identifier
                s"$QuoteId$QuoteId"
            }
        }
    }

    /** Format an SQL parameter name
     *
     *  This removes invalid characters.  It does not enclose the string
     *  in braces.
     *
     *  @param s Parameter
     *  @return  SQL parameter name as a string
     */
    final def sqlParameterName(s:String) : String = {
        s.replaceAll(CharsToStripFromPlaceholder, "")
    }

    /** Create list of named parameters
     *
     *  @param tuples Seq of tuples of form (column, value)
     *  @return       Seq of named parameters
     */
    def sqlNamedParameters(tuples:Seq[(String, String)]) : Seq[NamedParameter] = {
        tuples.foldLeft(Seq.empty[NamedParameter])(
            (accumulator, x) => {
                x match {
                    case (col:String, "\\N")        => (new NamedParameter(name = sqlParameterName(col), value = Option.empty[String])) +: accumulator
                    case (col:String, value:String) => (new NamedParameter(name = sqlParameterName(col), value = value)) +: accumulator
                }
            }
        )
    }

    /* ============================================================== */
    /* SQL statements                                                 */
    /* ============================================================== */

    /** Create a table
     *
     *  @param table        Table name
     *  @param columns      List of (column name, data type) tuples
     *  @param dbConnection Database connection
     *  @return             Unit or an exception
     */
    def createTable(table:String, columns:Seq[(String, String)])(implicit dbConnection:DbConnection) : Try[Unit] = {
        val columnList = columns.foldLeft("")((accumulator, x) => accumulator + s"|    ${sqlQuoteIdentifier(x._1)} ${x._2},\n").stripSuffix(",\n")

        Try {
            SQL(s"""
                |CREATE TABLE ${sqlQuoteIdentifier(table)} (
                $columnList
                |)
            """.stripMargin.trim).
                asSimple().
                execute()

            ()
        }
    }

    /** Execute a query statement
     *
     *  @param sql          SQL query statement
     *  @param dbConnection Database connection
     *  @return             List of rows as a Seq of name-value tuples, or an exception
     */
    def execute(sql:String)(implicit dbConnection:DbConnection) : Try[List[Seq[(String, Any)]]] = {
        // A generic row parser
        val rowParser = anorm.SqlParser.folder(Map.empty[String, Any]) { (map, value, meta) =>
            Right(map + (meta.column.alias.getOrElse(meta.column.qualified) -> value))
        }

        Try {
            val result = SQL(sql).
                asSimple().
                executeQuery()

            for {
                row <- result.as(rowParser.*)
            } yield row.toSeq
        }
    }

    /** Insert a row
     *
     *  @param table        Table name
     *  @param columns      Columns in form (name, dataType)
     *  @param values       Named parameter
     *  @param dbConnection Database connection
     *  @return             Unit or an exception
     */
    def insert(table:String, columns:Seq[(String, String)], values:Seq[String])(implicit dbConnection:DbConnection) : Try[Unit] = {
        val columnNames = sqlInsertColumns(columns.map(_._1))
        val columnValuePlaceholders = sqlInsertValueParameters(columns.map(_._1))
        Try {
            SQL(s"""
                |INSERT INTO ${sqlQuoteIdentifier(table)}
                |    ($columnNames)
                |VALUES
                |    ($columnValuePlaceholders)
            """.stripMargin.trim).
                asSimple().
                on(sqlNamedParameters(columns.map(_._1) zip values):_*).
                execute()

            ()
        }
    }
}
