/*
 * Copyright (c) 2017-2019 Lyle Frost <lfrost@cnz.com>.
 */

package main

import scala.util.Try

import com.github.tototoshi.csv.{CSVReader, DefaultCSVFormat, QUOTE_NONE, QUOTE_NONNUMERIC, Quoting}

/** CSV file format specification
 *
 *  @param delimiter           Column delimiter
 *  @param quoteChar           Quote character
 *  @param escapeChar          Escape character
 *  @param lineTerminator      EOL string
 *  @param quoting             Type of quoting
 *  @param treatEmptyLineAsNil Discard empty lines
 */
case class CsvFormat(
    override val delimiter           : Char    = ',',
    override val quoteChar           : Char    = '"',
    override val escapeChar          : Char    = '"',
    override val lineTerminator      : String  = "\n",
    override val quoting             : Quoting = QUOTE_NONNUMERIC,
    override val treatEmptyLineAsNil : Boolean = true
) extends DefaultCSVFormat

/** Process CSV input files
 *
 *  Column headers will be used as database column names.  The CSV
 *  column name may contain a data type by appending ::dataType to the
 *  name.
 */
object Input {
    /** Data type separator
     */
    val Sep = "::"

    /**
     */
    val CsvFormatCsv = CsvFormat()

    /**
     */
    val CsvFormatTsv = CsvFormat(
        delimiter  = '\t',
        escapeChar = '\\',
        quoting    = QUOTE_NONE
    )

    /** Open a CSV input file.
     *
     *  @param pathname    CSV file path
     *  @param charset     Character set name
     *  @param defaultType Default SQL data type
     *  @param csvFormat   CSV format
     *  @param eolStyle    EOL style
     *  @return            Tuple of seq of columns and CSV row iterator, with each column as a tuple of (name, data type)
     */
    def open(pathname:String, charset:String = "utf=8", defaultType:String)(implicit csvFormat:CsvFormat, eolStyle:EolStyle) : Try[(CSVReader, Seq[(String, String)], Iterator[Seq[String]])] = Try {
        val reader = CSVReader.open(filename = pathname, encoding = charset)(csvFormat.copy(lineTerminator = eolStyle.value))

        val ri = reader.iterator
        val headers = ri.next map {
            (s) => s.split(Sep, 2) match {  // scalastyle:ignore magic.number
                case Array(column)           => (column, defaultType)
                case Array(column, dataType) => (column, dataType)
            }
        }

        (reader, headers, ri)
    }
}
