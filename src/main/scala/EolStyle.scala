/*
 * Copyright (c) 2017-2019 Lyle Frost <lfrost@cnz.com>.
 */

package main

/** End-of-line style
 */
sealed trait EolStyle {
    /** Name of style
     */
    val name  : String

    /** EOL string value for this style
     */
    val value : String
}

/** Unix EOL style
 */
case object EolUnix extends EolStyle {
    override val name  = "Unix"
    override val value = "\n"
}

/** MacOS EOL style
 */
case object EolMac extends EolStyle {
    override val name  = "Mac"
    override val value = "\r"
}

/** DOS EOL style
 */
case object EolDos extends EolStyle {
    override val name  = "DOS"
    override val value = "\r\n"
}
