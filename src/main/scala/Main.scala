/*
 * Copyright (c) 2017-2019 Lyle Frost <lfrost@cnz.com>.
 */

package main

import java.io.{File, FileNotFoundException}
import java.nio.charset.{Charset, MalformedInputException, UnsupportedCharsetException}

import scala.io.{Codec, StdIn}
import scala.util.{Failure, Success, Try}
import scala.util.control.NonFatal

/** CLI configuration
 *
 *  @param charset  Character set
 *  @param csvs     CSV input files
 *  @param eolstyle EOL style
 *  @param format   Input file format
 *  @param verbose  Verbose flag
 */
case class CliConfig(
    charset  : Charset      = (Charset forName "utf-8"),
    csvs     : Vector[File] = Vector.empty[File],
    eolstyle : EolStyle     = EolUnix,
    format   : CsvFormat    = Input.CsvFormatCsv,
    verbose  : Boolean      = false
)

/** Starter template for Scala command-line applications.
 */
object Main extends App with ExitStatus {
    private val Pkg         = getClass.getPackage
    private val AppName     = Pkg.getImplementationTitle
    private val AppVersion  = Pkg.getImplementationVersion
    private val AppCommand  = AppName
    private val Prompt      = s"$AppName> "

    // CLI parser
    private val Parser = new scopt.OptionParser[CliConfig](AppCommand) {
        // Specify the application name and version for the help header.
        head(AppName, AppVersion)

        // Enable --help.
        help("help").
            text("prints this usage text")

        // Enable --version.
        version("version").
            text("display version then exit")

        opt[Unit]("verbose").
            text("verbose output").
            action((_, c) => c.copy(verbose = true))

        opt[String]("charset").
            text("character set, with utf-8 as the default").
            validate(x =>
                Try(new Codec(Charset forName x)) match {
                    case Success(_) => success
                    case Failure(e) => {
                        e match {
                            case _:UnsupportedCharsetException => failure(s"Invalid character set $x")
                            case NonFatal(e)                   => failure(s"Error processing charset $x: ${e.getMessage()}")
                        }
                    }
                }
            ).
            action((x, c) => c.copy(charset = (Charset forName x)))

        opt[String]("eolstyle").
            text("EOL style (unix|mac|dos), with unix as the default").
            validate(x => if (Seq("unix", "mac", "dos").contains(x.toLowerCase)) success else failure(s"Invalid eolstyle $x")).
            action((x, c) => {
                x.toLowerCase match {
                    case "unix" => c.copy(eolstyle = EolUnix)
                    case "mac"  => c.copy(eolstyle = EolMac)
                    case "dos"  => c.copy(eolstyle = EolDos)
                    case _      => c
                }
            })

        opt[String]("format").
            text("input file format (csv|tsv), with csv as the default").
            validate(x => if (Seq("csv", "tsv").contains(x.toLowerCase)) success else failure(s"Invalid eolstyle $x")).
            action((x, c) => {
                x.toLowerCase match {
                    case "csv" => c.copy(format = Input.CsvFormatCsv)
                    case "tsv" => c.copy(format = Input.CsvFormatTsv)
                    case _     => c
                }
            })

        arg[File]("<csvFile> ...").
            text("CSV input files").
            minOccurs(1).
            unbounded().
            validate(x =>
                if (x.exists) {
                    success
                } else {
                    failure(s"CSV input file ${x.getPath()} does not exist")
                }
            ).
            action((x, c) => c.copy(csvs = c.csvs :+ x))
    }

    // Parse the command line.
    Parser.parse(args, CliConfig()) match {
        case Some(cliConfig) => {
            implicit val eolstyle  = cliConfig.eolstyle
            implicit val csvFormat = cliConfig.format

            implicit val conn = Sql.connect() match {
                case Success(c) => c
                case Failure(e) => {
                    Console.println(s"Error connecting to the database: ${e.getMessage()}")
                    sys.exit(ExitFailure)
                }
            }

            // Process each input CSV.
            cliConfig.csvs foreach { csv =>
                val pathname = csv.getAbsolutePath()
                val filename = csv.getName()
                if (cliConfig.verbose) Console.println(s"Processing CSV file $filename.")
                val table:String = filename.split("[-\\.]").toSeq match {
                    case head +: _ => head
                    case _         => {
                        Console.println(s"Unable to extract table name from filename $filename.")
                        sys.exit(ExitFailure)
                    }
                }
                Try {
                    // val inputIterator = Source.fromFile(pathname).getLines()
                    val (reader, columns, inputIterator) = Input.open(pathname = pathname, charset = cliConfig.charset.name(), defaultType = Sql.DefaultType) match {
                        case Success(x) => x
                        case Failure(e) => {
                            Console.println(s"Error opening CSV file $pathname: ${e.getMessage}")
                            sys.exit(ExitFailure)
                        }
                    }
                    if (cliConfig.verbose) Console.println(s"Creating table $table.")
                    Sql.createTable(table, columns) match {
                        case Success(_) => ()
                        case Failure(e) => {
                            Console.println(s"Error creating table $table: ${e.getMessage}")
                            sys.exit(ExitFailure)
                        }
                    }
                    // Process each line of input.
                    inputIterator.
                        takeWhile(row => row != null).
                        foreach { row =>
                            if (cliConfig.verbose) Console.print(".")
                            Sql.insert(table, columns, row) match {
                                case Success(_) => ()
                                case Failure(e) => {
                                    Console.println(s"Error inserting into table $table: ${e.getMessage}")
                                    sys.exit(ExitFailure)
                                }
                            }
                        }
                    if (cliConfig.verbose) Console.println("")  // newline after the dots
                    reader.close()
                } match {
                    case Success(_) => {
                        ()
                    }
                    case Failure(e) => {
                        e match {
                            case _:FileNotFoundException   => Console.println(s"The input file $csv was not found.")
                            case _:MalformedInputException => Console.println(s"Error reading $csv.  Likely ${cliConfig.charset} is not the correct character set.")
                            case NonFatal(e)               => Console.println(e)
                        }
                        sys.exit(ExitFailure)
                    }
                }
            }

            // Default to stdin.  Mute prompt if input is not from the console.
            val prompt = if (java.lang.System.console() == null) "" else Prompt
            Iterator.continually(StdIn.readLine(prompt)).
                takeWhile(line => line != null).
                foreach {
                    line => ProcessLine(line) match {
                        case Success(true)  => sys.exit(ExitFailure)
                        case Success(false) => ()
                        case Failure(e)     => {
                            Console.println(s"Error running command:  ${e.getMessage()}")
                        }
                    }
                }
        }
        case None            => {
            // Invalid command line.  Error message will have been displayed by scopt.
            sys.exit(ExitUsage)
        }
    }
}
