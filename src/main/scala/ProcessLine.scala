/*
 * Copyright (c) 2017-2019 Lyle Frost <lfrost@cnz.com>.
 */

package main

import java.sql.{Connection => DbConnection}

import scala.util.{Failure, Success, Try}

/** Singleton for processing a line of input.
 */
object ProcessLine {
    /** Process a line of input.
     *
     *  @param line         Line to be parsed
     *  @param dbConnection Database connection
     *  @return             true to exit or an exception
     */
    def apply(line:String)(implicit dbConnection:DbConnection) : Try[Boolean] = {
        line.toLowerCase.trim.stripSuffix(".") match {
            case ""                               => Success(false)
            case "\\q" | "exit" | "done" | "quit" => Success(true)
            case "?" | "help"                     => {
                Console.println("""
                    |help   Display this help information
                    |shell  Launch an SQL shell
                    |web    Launch a SQL GUI client in a browser.
                    |exit   Exit the application
                """.stripMargin.trim)
                Success(false)
            }
            case "shell"                          => {
                Sql.shell() match {
                    case Success(()) => Success(false)
                    case Failure(e)  => Failure(e)
                }
            }
            case "web"                            => {
                Console.println("Press the disconnect button in the toolbar at the top of the browser when done.")
                Sql.web() match {
                    case Success(()) => Success(false)
                    case Failure(e)  => Failure(e)
                }
            }
            case s:String                         => Failure(new Exception(s"Invalid command $s."))
        }
    }
}
