/*
 * Copyright (c) 2017-2019 Lyle Frost <lfrost@cnz.com>.
 */

name := "sql4csv"

version := "1.1.0"

val gitBaseUrl = settingKey[String]("Git base URL")
gitBaseUrl := "https://gitlab.com/lfrost"

scalaVersion := "2.13.1"

// https://docs.scala-lang.org/overviews/compiler-options/
scalacOptions ++= Seq(
    // Standard Settings
    "-deprecation",             // Emit warning and location for usages of deprecated APIs.
    "-encoding", "utf-8",       // Specify character encoding used by source files.
    "-explaintypes",            // Explain type errors in more detail.
    "-feature",                 // Emit warning and location for usages of features that should be imported explicitly.
    "-unchecked",               // Enable additional warnings where generated code depends on assumptions.

    // Advanced Settings

    // Warning Settings
    "-Ywarn-dead-code",         // Warn when dead code is identified.
    "-Ywarn-value-discard",     // Warn when non-Unit expression results are unused.
    "-Ywarn-numeric-widen",     // Warn when numerics are widened.
    "-Ywarn-unused:_",          // Enable or disable specific unused warnings: _ for all, -Ywarn-unused:help to list choices.
    "-Ywarn-extra-implicit",    // Warn when more than one implicit parameter section is defined.
    "-Xlint:_"                  // Enable or disable specific warnings: _ for all, -Xlint:help to list choices.
)

scalacOptions in (Compile, doc) ++=
    Opts.doc.title(name.value)                                                                                             ++
    Opts.doc.sourceUrl(gitBaseUrl.value + "/" + name.value + "/tree/" + git.gitCurrentBranch.value + "€{FILE_PATH}.scala") ++
    Seq("-sourcepath", baseDirectory.value.getAbsolutePath)

libraryDependencies ++= Seq(
    "com.h2database"          %  "h2"        % "1.4.200",
    "com.github.scopt"        %% "scopt"     % "3.7.1",
    "com.github.tototoshi"    %% "scala-csv" % "1.3.6",
    "org.playframework.anorm" %% "anorm"     % "2.6.5",
    "org.scalatest"           %% "scalatest" % "3.1.0" % "test"
)

// Silence eviction warnings
// evictionWarningOptions in update := EvictionWarningOptions.default.withWarnTransitiveEvictions(false)

import sbtassembly.AssemblyPlugin.defaultShellScript

assemblyOption in assembly := (assemblyOption in assembly).value.copy(prependShellScript = Some(defaultShellScript))

assemblyJarName in assembly := s"${name.value}-${version.value}"

test in assembly := {}
